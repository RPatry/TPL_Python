""" Module de manipulations geometriques """


class Vector:
    """ Represente un vecteur dans l'espace.
    Il est possible de l'additionner ou de le soustraire avec un autre
    vecteur, ou de le multiplier avec un scalaire.
    """

    # pylint: disable=invalid-name
    # => desactive pour autoriser les noms de proprietes comme x, y et z

    def __init__(self, x, y, z):
        self.__x = x
        self.__y = y
        self.__z = z

    @property
    def x(self):
        """ retourne la coordonnee x du vecteur """
        return self.__x

    @property
    def y(self):
        """ retourne la coordonnee y du vecteur """
        return self.__y

    @property
    def z(self):
        """ retourne la coordonnee z du vecteur """
        return self.__z

    def __str__(self):
        return '({},{},{})'.format(self.x, self.y, self.z)

    def __add__(self, vect2):
        if not isinstance(vect2, Vector):
            return None
        return Vector(self.__x + vect2.x, self.__y + vect2.y,
                      self.__z + vect2.z)

    def __sub__(self, vect2):
        if not isinstance(vect2, Vector):
            return None
        return Vector(self.__x - vect2.x, self.__y - vect2.y,
                      self.__z - vect2.z)

    def __mul__(self, scalar):
        return Vector(self.__x * scalar, self.__y * scalar, self.__z * scalar)

    def __rmul__(self, scalar):
        return self * scalar


class Triangle:
    """ Represente un triangle d'un fichier STL (trois sommets associes a une
    normale) """

    def __init__(self, vertex1, vertex2, vertex3, normale):
        self.__vertex1 = vertex1
        self.__vertex2 = vertex2
        self.__vertex3 = vertex3
        self.__normale = normale

    @property
    def normale(self):
        """ Retourne le vecteur normal au triangle, qui determine sa surface
        """
        return self.__normale

    @property
    def vertex1(self):
        """ Retourne le premier sommet du triangle """
        return self.__vertex1

    @property
    def vertex2(self):
        """ Retourne le second sommet du triangle """
        return self.__vertex2

    @property
    def vertex3(self):
        """ Retourne le troisieme sommet du triangle """
        return self.__vertex3

    @property
    def vertices(self):
        """ Retourne la liste des sommets du triangle """
        return [self.__vertex1, self.__vertex2, self.__vertex3]

    def __str__(self):
        return '<Normale: {}, vertices: [{}, {}, {}]>'.format(
            self.__normale, self.__vertex1, self.__vertex2, self.__vertex3)


def dot_product(vect1, vect2):
    """ Retourne le produit scalaire de deux vecteurs """
    return (vect1.x * vect2.x +
            vect1.y * vect2.y +
            vect1.z * vect2.z)


def iterate_over_all_z(triangles):
    """ Itere sur toutes les coordonnees z de tous les sommets d'une liste
    de triangles. """
    for tri in triangles:
        yield tri.vertex1.z
        yield tri.vertex2.z
        yield tri.vertex3.z


def iterate_over_all_x(triangles):
    """ Itere sur toutes les coordonnees x de tous les sommets d'une liste
    de triangles. """
    for tri in triangles:
        yield tri.vertex1.x
        yield tri.vertex2.x
        yield tri.vertex3.x


def iterate_over_all_y(triangles):
    """ Itere sur toutes les coordonnees x de tous les sommets d'une liste
    de triangles. """
    for tri in triangles:
        yield tri.vertex1.y
        yield tri.vertex2.y
        yield tri.vertex3.y


def z_min(triangles):
    """ Trouver la coordonnee z la plus petite dans une liste de triangles"""
    return min(iterate_over_all_z(triangles))


def z_max(triangles):
    """ Trouver la coordonnee z la plus grande dans une liste de triangles"""
    return max(iterate_over_all_z(triangles))


def x_min(triangles):
    """ Trouver la coordonnee x la plus petite dans une liste de triangles"""
    return min(iterate_over_all_x(triangles))


def x_max(triangles):
    """ Trouver la coordonnee x la plus grande dans une liste de triangles"""
    return max(iterate_over_all_x(triangles))


def y_min(triangles):
    """ Trouver la coordonnee y la plus petite dans une liste de triangles"""
    return min(iterate_over_all_y(triangles))


def y_max(triangles):
    """ Trouver la coordonnee y la plus grande dans une liste de triangles"""
    return max(iterate_over_all_y(triangles))


def triangles_crossing_plane(triangles, plane):
    """ Iterer sur une liste de triangles en intersection avec un plan
    (sans etre non plus contenus dans ce plan)"""
    for triangle in triangles:
        res1 = plane.plane_equation(triangle.vertex1)
        res2 = plane.plane_equation(triangle.vertex2)
        res3 = plane.plane_equation(triangle.vertex3)
        inside_plane = res1 == 0.0 and res2 == 0.0 and res3 == 0.0
        outside_plane = res1 < 0 == res2 < 0 and res2 < 0 == res3 < 0
        if not (inside_plane or outside_plane):
            yield triangle


def linspace(low, high, nb_div):
    """ Retourne une liste de nb_div valeurs, espacees de maniere egale sur
    l'intervalle ]low; high[.
    Exemple: linspace(-2, 3, 9) donne [-1.5, -1, -0.5, 0, 0.5, 1, 1.5, 2, 2.5]
    """
    nb_div = int(nb_div)
    if high <= low or nb_div <= 0:
        return None
    largeur_intervalle = abs(high) + abs(low)
    largeur_sous_intervalles = largeur_intervalle / (nb_div + 1)
    borne = low
    # Le calcul par nombre flottant force l'apparition d'erreur d'approximation
    valeurs = []
    for _ in range(nb_div):
        borne += largeur_sous_intervalles
        valeurs.append(borne)
    return valeurs


# pylint: disable=invalid-name
def get_intersection_points_triangle_and_plane(triangle, plane):
    """ Renvoie une liste de points d'intersection entre un plan et un triangle
    Precondition: le plan doit intersecter le triangle, sans le contenir """

    points = []  # les points d'intersection entre plan et triangle
    vert1, vert2, vert3 = triangle.vertex1, triangle.vertex2, triangle.vertex3

    # Calculer les resultats de l'equation ax+by+cz+d pour tous les sommets
    res1 = plane.plane_equation(vert1)
    res2 = plane.plane_equation(vert2)
    res3 = plane.plane_equation(vert3)

    # On associe les sommets avec leurs resultats respectifs
    # de l'equation du plan
    vert_res_1 = vert1, res1
    vert_res_2 = vert2, res2
    vert_res_3 = vert3, res3

    # Verifier d'abord si certains des sommets appartiennent au plan
    for (vertex, res_equation) in (vert_res_1, vert_res_2, vert_res_3):
        if res_equation == 0:  # si le sommet est contenu dans le plan
            points.append(vertex)

    # Pas d'intersection significative entre sommets et plan:
    # => On doit trouver les points d'intersection sur les segments du triangle
    if len(points) < 2:
        for (vert1, res1), (vert2, res2) in ((vert_res_1, vert_res_2),
                                             (vert_res_2, vert_res_3),
                                             (vert_res_3, vert_res_1)):
            if (res1 < 0) != (res2 < 0) and res1 != 0 and res2 != 0:
                intersection = plane.intersection_line_plane(vert1, vert2)
                points.append(intersection)
    return points


class Plane:
    """ Represente un plan de l'espace defini par une equation
    cartesienne (ax+by+cz+d=0, avec (a,b,c) le vecteur normal du plan) """

    def __init__(self, ax=0, by=0, cz=0, d=0):
        self.normal = Vector(ax, by, cz)
        self.__d = d

    def plane_equation(self, point):
        """ Calcule la valeur de l'equation cartesienne du plan pour un
        point donne.
        """
        return dot_product(self.normal, point) + self.__d

    def intersection_line_plane(self, point1, point2):
        """ Calcule un point d'intersection entre une ligne d'un segment
        et le plan"""

        # on calcule l'intersection en considerant l'equation du plan
        # ainsi que la representation parametrique de la ligne du segment
        line_normal = point2 - point1
        dividende = self.plane_equation(point1)
        diviseur = dot_product(self.normal, line_normal)
        param = dividende / diviseur
        return point1 + param * line_normal
