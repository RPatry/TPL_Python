""" Module de lecture de fichiers STL au format binaire """

import os
import struct
from geometrie import Vector, Triangle


def file_ok(path):
    """ Verifie qu'un chemin vers un fichier STL satisfie les bonnes proprietes
    (le chemin existe, le fichier est lisible, etc) """
    if not os.path.isfile(path):
        return False
    try:
        with open(path, 'rb'):
            return True
    except IOError:
        return False  # l'ouverture a echoue, le fichier n'est pas lisible


def bytes_to_float(bytes_array):
    """ Convertit un tableau d'octets en un nombre flottant """
    # '<f' signifie : un nombre flottant en little endian
    return struct.unpack('<f', bytes_array)[0]


def bytes_to_ascii(bytes_array):
    """ Convertit un tableau d'octets en une chaine Python, en interpretant
    les octets comme codant de l'ASCII """
    return bytes_array.decode('ascii')


def read_stl_file(path):
    """ Lire un fichier STL binaire: retourne la liste des triangles
    qu'il contient. Renvoie None en cas d'erreur.
    """
    if not file_ok(path):
        return None
    with open(path, "rb") as stl_file:
        stl_file.read(80)  # on saute le header

        nb_triangles_bytes = stl_file.read(4)
        nb_triangles = int.from_bytes(nb_triangles_bytes, byteorder='little')
        triangles = []
        for _ in range(nb_triangles):
            vertices = []
            for _ in range(4):  # 4 vecteurs: la normale et les 3 vertices
                floats = []
                for _ in range(3):  # 3 composantes pour chaque vecteur
                    floats.append(bytes_to_float(stl_file.read(4)))
                vertex = Vector(floats[0], floats[1], floats[2])
                vertices.append(vertex)
            triangle = Triangle(vertices[1], vertices[2], vertices[3],
                                vertices[0])
            triangles.append(triangle)
            stl_file.read(2)  # on ignore le 'Attribute byte count'

        return triangles
