""" Decoupage de fichiers SVG - Logique principale """

from multiprocessing import Pool, RLock
from stl import read_stl_file
from geometrie import linspace, z_min, z_max, Plane, triangles_crossing_plane
from geometrie import Vector, get_intersection_points_triangle_and_plane
from geometrie import x_min, x_max, y_min, y_max
from svg import SVGFile
from utils import get_nb_digits, int_to_string_with_leading_zeroes


# La generation des fichiers SVG est un probleme qualifie
# "embarassingly parrallel", il n'y a aucune dependance entre
# les taches, si ce n'est l'acces a la sortie standard,
# pour indiquer la fin de la tache. L'acces a la sortie
# standard se controle grace a ce verrou.
LOCK_PRINT = RLock()


# pylint: disable=too-many-arguments
def slice_stl_one_plane(triangles, z_coordinate_plane, name_output_file,
                        minx, maxx, miny, maxy):
    """ Determiner l'intersection entre le modele STL et
    un plan unique;
    Inscrire cette intersection sous forme d'un fichier SVG
    """
    plane = Plane(ax=0, by=0, cz=1, d=z_coordinate_plane * -1)
    # le point de translation nous permet de nous ramener a un
    # plan 2D d'origine (0, 0) dans tous les cas
    point_translation = Vector(x=minx*-1, y=miny*-1, z=0)
    svg_output = SVGFile(width_initial=maxx-minx, height_initial=maxy-miny)
    for triangle in triangles_crossing_plane(triangles, plane):
        points = get_intersection_points_triangle_and_plane(triangle, plane)
        if len(points) != 2:
            continue
        pt1 = points[0] + point_translation
        pt2 = points[1] + point_translation
        svg_output.add_line(pt1, pt2)
    with open(name_output_file, 'w') as file_svg:
        file_svg.write(svg_output.write())
    # Le verrou permet d'eviter un entrelacement des affichages entre
    # les differents processus
    LOCK_PRINT.acquire()
    print(name_output_file, " est pret!")
    LOCK_PRINT.release()


# pylint: disable= too-many-locals
def slice_stl_file(path_stl, base_name_output_file, nb_planes, nb_proc):
    """ Decoupe un fichier STL en plusieurs plans.
    Retourne un message d'erreur en cas d'erreur
    """

    triangles = read_stl_file(path_stl)
    if triangles is None:  # Test si erreur pendant lecture du fichier STL
        return "{} n'est pas un fichier lisible!".format(path_stl)

    minz, maxz = z_min(triangles), z_max(triangles)
    minx, maxx = x_min(triangles), x_max(triangles)
    miny, maxy = y_min(triangles), y_max(triangles)

    z_coords = linspace(minz, maxz, nb_planes)

    nb_digits = get_nb_digits(nb_planes)
    args_for_processes = []
    for index, z_plane in enumerate(z_coords):
        output_file_path = ''.join(
            (base_name_output_file,
             "_",
             int_to_string_with_leading_zeroes(index, nb_digits),
             '.svg'))
        args_for_processes.append(
            (triangles, z_plane, output_file_path,
             minx, maxx, miny, maxy)
        )

    # Execution parallele si le nombre de processus donne en argument est > 1
    with Pool(nb_proc) as pool:
        pool.starmap(slice_stl_one_plane, args_for_processes)

    print("c'est fini!")

    return ""  # Pas d'erreur
