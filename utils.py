""" Diverses fonctions utilitaires """

import os


def is_int_convertible(value):
    """ Verifier si une valeur peut se convertir en entier """
    try:
        int(value)
        return True
    except ValueError:
        return False


def assert_or_exit(assertion, error_message):
    """ Quitte le programme et affiche un message d'erreur si une assertion
    n'est pas verifiee """
    if not assertion:
        print("Erreur: ", error_message)
        exit(1)


def get_nb_digits(number):
    """ Retourne le nombre de digits contenus dans la partie entiere
    d'un nombre """
    from math import log, floor
    return 1 + floor(log(number, 10))


def create_dir_if_needed(directory):
    """ Creer le dossier de destination si necessaire.
    Renvoie un message d'erreur si le dosier n'a pas pu
    etre cree, ou si on ne peut pas ecrire dedans.
    """
    if os.path.isdir(directory):
        if os.access(directory, os.W_OK):
            return ""
        else:
            return "{} n'est pas un dossier avec droits d'ecriture".format(
                    directory)
    try:
        os.makedirs(directory)
    except PermissionError:
        return "{} n'a pas pu etre cree!".format(directory)
    return ""


# pylint a beau se plaindre, reduire la longueur du nom troublerait sa
# comprehension
# pylint: disable=invalid-name
def int_to_string_with_leading_zeroes(number, nb_digits):
    """ Retourne la representation textuelle d'un nombre en ajoutant si
    necessaire des zeros devant"""
    fmt = '%0' + str(nb_digits) + 'd'
    return (fmt) % number
