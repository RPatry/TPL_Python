#!/usr/bin/env python3

""" Point d'entree du projet """

# pylint: disable=invalid-name

from argparse import ArgumentParser
import os.path
from decoupe import slice_stl_file
from utils import is_int_convertible, assert_or_exit, create_dir_if_needed


def parse_arguments():
    """ Recolter des arguments sur la ligne de commande """
    args_parser = ArgumentParser(description='Decoupe un ' +
                                 'fichier STL en tranches (inscrites dans des'
                                 + ' fichiers SVG)')
    args_parser.add_argument('--source', metavar='FIC_STL_SOURCE',
                             required=True,
                             help='Chemin vers le fichier STL')
    args_parser.add_argument('--dest', metavar='DOSSIER_DESTINATION',
                             help='Dossier de stockage des images SVG' +
                             ' (par defaut le dossier du fichier STL)',
                             default='.')
    args_parser.add_argument('--version', action='version',
                             version='%(prog)s 1.0')
    args_parser.add_argument('--proc', metavar='NB_PROCS',
                             help='Nombre de processeurs a exploiter'
                             + ' (1 par defaut)',
                             default=1)
    args_parser.add_argument('-s', metavar='NB_PLANES',
                             help='Nombre de plans pour decouper' +
                             ' (20 par defaut)', default=20)
    args_parser.add_argument('--basename', metavar='SVG_BASENAME',
                             help='Un prefixe qui sera utilise pour nommer ' +
                             'les fichiers SVG generes. Par defaut, le nom ' +
                             'du fichier STL est utilise.')

    parsed_args = args_parser.parse_args()
    dict_args = vars(parsed_args)
    return dict_args


def get_filename_without_extension(path):
    """ Obtenir le nom d'un fichier, sans l'extension ni le chemin
    Exemple: retourne "monfic" si on passe "/tmp/dossier/monfic.txt"
    """
    base = os.path.basename(path)
    return os.path.splitext(base)[0]


if __name__ == '__main__':
    ARGS = parse_arguments()
    if ARGS['basename'] is None:
        ARGS['basename'] = get_filename_without_extension(ARGS['source'])
    assert_or_exit(is_int_convertible(ARGS['proc']) and int(ARGS['proc']) >= 1,
                   "Le nombre de processeurs doit etre positif!")
    assert_or_exit(is_int_convertible(ARGS['s']) and int(ARGS['s']) >= 1,
                   "Le nombre de plans doit etre positif!")
    DIR_WRITE = create_dir_if_needed(ARGS['dest'])
    assert_or_exit(len(DIR_WRITE) == 0, DIR_WRITE)
    ERROR_MESS = slice_stl_file(ARGS['source'],
                                os.path.join(ARGS['dest'], ARGS['basename']),
                                int(ARGS['s']),
                                int(ARGS['proc']))
    assert_or_exit(len(ERROR_MESS) == 0, ERROR_MESS)
    print('Fini!')
