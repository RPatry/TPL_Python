""" Module d'ecriture de fichiers SVG """


class SVGFile:
    """ Represente un dessin au format SVG, pour s'abstraire de l'ecriture
    des chaines directement """

    def __init__(self, width_initial=500, height_initial=500):
        self.__lines = []
        self.width = width_initial
        self.height = height_initial

    def add_line(self, point1, point2):
        """ ajouter une ligne au flux svg """
        fmt = ("<line x1='{}' y1='{}' x2='{}' y2='{}' style='stroke:" +
               "rgb(255, 0, 0);stroke-width:5%;' />")
        self.__lines.append(fmt.format(point1.x, point1.y, point2.x, point2.y))

    def write(self):
        """ retourne une chaine en syntaxe svg prete a etre ecrite dans un
        fichier
        """
        string = '<svg width="{}" height="{}">\n'.format(
            self.width, self.height)

        string += '\n'.join(self.__lines)
        return string + "</svg>"
